import Features.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Login.class,
        GetWorkspace.class,
        SelectWorkspace.class,
        GetGroup1.class,
        GetGroup2.class,
        GetGroupTree.class,
//        CreateGroup.class,
//        UpdateGroup.class,
//        DeleteGroup.class,
        GetUsersInGroup.class,
        GetUsersInWorkspace.class,
        GetAllUsersInWorkspace.class,
        AddUser.class,
        RemoveUser.class,
        GetRoleUsers.class,
        GetApplicationsAndDatastores.class,
        AddRoleToUser.class,
        RemoveRoleFromUser.class,
})

public class AllApiTest {

}
