package Database;

import Data.*;

public class Database {

    public static Account account = new Account();
    public static Token token = new Token();
    public static Current_workspace_id id = new Current_workspace_id();
    public static Groupid groupid = new Groupid();
    public static Group_name groupName = new Group_name();
    public static New_Group newGroup = new New_Group();
    public static User user = new User();
    public static DeleteUser deleteUser = new DeleteUser();
    public static RoleUser roleUser = new RoleUser();
    public static Add_Role_ToUser addrole = new Add_Role_ToUser();
}
