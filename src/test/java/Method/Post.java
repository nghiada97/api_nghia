package Method;


import Data.Account;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.RestAssured;

public class Post {

    public Response PostAcount(Account account){
            return RestAssured.given().log().all()
                    .contentType(ContentType.JSON)
                    .when()
                    .body(account)
                    .post();
    }

    public Response Post(String a, String token){
        return RestAssured.given().log().all()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .when()
                .body(a)
                .post();
    }
}
