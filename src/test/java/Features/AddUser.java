package Features;

import Database.Database;
import Method.Post;
import io.restassured.response.Response;
import org.junit.Test;

public class AddUser {

    private Post post = new Post();

    @Test
    public void AddUser(){
        Database.user.setEmail("maimai@gmail.com");
        Database.user.setUsername("MaiMai");
        Database.user.setG_id(Database.groupid.getGroup_id());
        Database.user.setW_id(Database.id.getWorkspace_id());

        Base.setPath("/api/v0/users");
        Response res = post.Post(Base.Convert(Database.user),Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();

        Database.deleteUser.setU_id(res.jsonPath().get("user_profile.u_id"));
    }

}
