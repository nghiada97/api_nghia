package Features;

import Database.Database;
import Method.Get;
import io.restassured.response.Response;
import org.junit.Test;

public class GetRoleUsers {

    private Get get = new Get();

    @Test
    public void GetRoleUser(){
        Base.setPath("/api/v0/applications/"+ Database.roleUser.getApp_id() +"/roleusers/"+Database.roleUser.getRole_id());

        Response res = get.Get(Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();

        Database.addrole.setUser_id(res.jsonPath().get("role_users.u_id[0]"));
        Database.addrole.setRole_id(Database.roleUser.getRole_id());
    }

}
