package Features;

import Data.Group_name;
import Database.Database;
import Method.Put;
import io.restassured.response.Response;
import org.junit.Test;

public class UpdateGroup {

    private Group_name groupName = new Group_name();
    private Put put = new Put();

    @Test
    public void UpdateGroup(){
        groupName.setName("MaiMai1");
        groupName.setDisplay_id("MaiMai1");

        Base.setPath("/api/v0/workspaces/"+ Database.id.getWorkspace_id() +"/groups/"+Database.newGroup.getGroup_id());
        Response res = put.Put(Base.Convert(groupName),Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();
    }
}
