package Features;

import Database.Database;
import Method.Post;
import io.restassured.response.Response;
import org.junit.Test;

public class CreateGroup {

    private Post post = new Post();

    @Test
    public void CreateGroup(){
        Database.groupName.setName("MaiMai");
        Database.groupName.setDisplay_id("MaiMai");

        Base.setPath("/api/v0/groups/"+ Database.groupid.getGroup_id());
        Response res = post.Post(Base.Convert(Database.groupName), Database.token.getToken());

        res.then().statusCode(200);
        res.prettyPrint();

        Database.newGroup.setGroup_id(res.jsonPath().get("group.g_id"));

    }
}
