package Features;

import Database.Database;
import Method.Delete;
import io.restassured.response.Response;
import org.junit.Test;

public class DeleteGroup {

    private Delete delete = new Delete();


    @Test
    public void DeleteGroup(){
        Base.setPath("/api/v0/groups/"+ Database.newGroup.getGroup_id());

        Response res = delete.Delete(Database.token.getToken());
        res.then().statusCode(200);
    }
}
