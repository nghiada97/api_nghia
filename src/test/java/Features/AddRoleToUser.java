package Features;

import Database.Database;
import Method.Post;
import io.restassured.response.Response;
import org.junit.Test;

public class AddRoleToUser {

    Post post = new Post();

    @Test
    public void AddRoleToUser(){
        Base.setPath("/api/v0/applications/"+ Database.roleUser.getApp_id() +"/userroles");

        Response res = post.Post(Base.Convert(Database.addrole),Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();
    }
}
