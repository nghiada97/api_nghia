package Features;

import Database.Database;
import Method.Delete;
import io.restassured.response.Response;
import org.junit.Test;

public class RemoveUser {

    private Delete delete = new Delete();

    @Test
    public void RemoveUser(){
        Database.deleteUser.setG_id(Database.groupid.getGroup_id());
        Database.deleteUser.setW_id(Database.id.getWorkspace_id());

        Base.setPath("/api/v0/users");
        Response res = delete.Delete1(Base.Convert(Database.deleteUser),Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();
    }

}
