package Features;

import Database.Database;
import Method.Post;
import io.restassured.response.Response;
import org.junit.Test;

public class SelectWorkspace {

    private Post post = new Post();

    @Test
    public void SelectWorkSpace(){
        Base.setPath("/api/v0/workspaces/"+ Database.id.getWorkspace_id() +"/select");

        Response res = post.Post("",Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();
    }
}
