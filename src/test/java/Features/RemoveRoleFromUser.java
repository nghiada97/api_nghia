package Features;

import Database.Database;
import Method.Delete;
import io.restassured.response.Response;
import org.junit.Test;

public class RemoveRoleFromUser {

    Delete delete = new Delete();

    @Test
    public void RemoveRoleFromUser(){
        Base.setPath("/api/v0/applications/"+ Database.roleUser.getApp_id() +"/userroles");

        Response res = delete.Delete1(Base.Convert(Database.addrole),Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();
    }
}
