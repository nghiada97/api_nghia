package Features;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.RestAssured;
import org.junit.Before;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Base {





    public static void setPath(String path){
        RestAssured.basePath = path;
    }

    public static String Convert(Object object)  {

        String str = null;

        ObjectMapper mapper = new ObjectMapper();
        try {
           str =  mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
       return str;
    }

    @Before
    public void init(){
        RestAssured.baseURI = "https://az-api.hexabase.com";

    }

}