package Features;

import Database.Database;
import Method.Get;
import io.restassured.response.Response;
import org.junit.Test;

public class GetGroup1 {

    private Get get = new Get();

    @Test
    public void GetGroup(){
        Base.setPath("/api/v0/groups");

        Response res = get.Get(Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();

        System.out.println(res.jsonPath().get("group.g_id").toString());

        Database.groupid.setGroup_id(res.jsonPath().get("group.g_id"));


    }
}
