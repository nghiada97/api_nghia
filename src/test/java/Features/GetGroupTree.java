package Features;

import Database.Database;
import Method.Get;
import io.restassured.response.Response;
import org.junit.Test;

public class GetGroupTree {

    private Get get = new Get();

    @Test
    public void GetGroupTree(){

        Base.setPath("/api/v0/grouptree");

        Response res = get.Get(Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();

    }

}
