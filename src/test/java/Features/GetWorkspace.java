package Features;

import Database.Database;
import Method.Get;
import io.restassured.response.Response;
import org.junit.Test;

public class GetWorkspace {

    private Get get  = new Get();

    @Test
    public void getWorkspace(){
        Base.setPath("/api/v0/workspaces");

        Response res = get.Get(Database.token.getToken());
        res.then().statusCode(200);

        res.prettyPrint();
        Database.id.setWorkspace_id(res.jsonPath().get("current_workspace_id"));
    }
}
