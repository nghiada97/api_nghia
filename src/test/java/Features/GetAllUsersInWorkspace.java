package Features;

import Database.Database;
import Method.Get;
import io.restassured.response.Response;
import org.junit.Test;

public class GetAllUsersInWorkspace {

    private Get get = new Get();

    @Test
    public void GetAllUsersInWorkspace(){
        Base.setPath("/api/v0/users/all/g/"+ Database.groupid.getGroup_id());

        Response res = get.Get(Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();

        Database.roleUser.setRole_id(res.jsonPath().get("members[0].user_roles[0].role_id"));
        Database.roleUser.setApp_id(res.jsonPath().get("members[0].user_roles[0].application_id"));


    }
}
