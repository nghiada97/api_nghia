package Features;

import Database.Database;
import Method.Get;
import io.restassured.response.Response;
import org.junit.Test;

public class GetApplicationsAndDatastores {

    private Get get = new Get();

    @Test
    public void GetApplicationAnDatastores(){
        Base.setPath("/api/v0/workspaces/"+ Database.id.getWorkspace_id() +"/applications");

        Response res = get.Get(Database.token.getToken());
        res.then().statusCode(200);
        res.prettyPrint();


    }

}
