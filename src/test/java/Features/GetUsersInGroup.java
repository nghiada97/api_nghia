package Features;

import Database.Database;
import Method.Get;
import io.restassured.response.Response;
import org.junit.Test;

public class GetUsersInGroup {

    private Get get = new Get();

    @Test
    public void GetUserInGroup(){
        Base.setPath("/api/v0/users/api/v0/groups/"+ Database.newGroup.getGroup_id()+"/users?recursive=true");

        Response res = get.Get(Database.token.getToken());
        res.then().statusCode(200);

    }
}
